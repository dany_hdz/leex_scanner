Definitions.

D = [0-9]
I = (((_[0-9]+)+)+)
L = [A-Za-z]


Rules.
% Rust docs: https://doc.rust-lang.org/reference/tokens.html

% Spaces
\s|\n|\t|\r : {token, {space, TokenLine, TokenChars}}.

% Keywords
as|break|const|continue|crate|else|enum|extern|false|fn|for|if|impl|in|let|loop|match|mod|move|mut|pub|ref|return|self|Self|static|struct|super|trait|true|type|unsafe|use|where|while|aync|await|dyn|abstract|become|box|do|final|macro|override|priv|typeof|unsized|virtual|yield|try : {token, {keyword, TokenLine, TokenChars}}.

% Datatypes
int|bool|char|float|f32|f64|i8|i16|i32|i64|i128|isize|u8|u16|u32|u64|u128|usize|Vec : {token, {datatype, TokenLine, TokenChars}}.

% Numbers
{D}+{I}? : {token, {int, TokenLine, TokenChars}}.
0x[A-Fa-f0-9]+(((_[A-Fa-f0-9]+)+)+)? : {token, {hex, TokenLine, TokenChars}}.
0o[0-7]+(((_[0-7]+)+)+)? : {token, {octal, TokenLine, TokenChars}}.
0b[0-1]+(((_[0-1]+)+)+)? : {token, {bin, TokenLine, TokenChars}}.
{D}+{I}?\.{D}*{I}?(E\+[0-9]+)*{I}?|{D}+(E\+[0-9]+)+ : {token, {float, TokenLine, TokenChars}}.

%[0-9](_?[0-9])* : {token, {prueba, TokenLine, TokenChars}}.

% Strings
'(.|\s)?' : {token, {char, TokenLine, TokenChars}}.
"([^"]|\\")*" : {token, {string, TokenLine, TokenChars}}.
r#"([^"]|\\")*"# : {token, {rawString, TokenLine, TokenChars}}.
b'(.|\s)?' : {token, {byte, TokenLine, TokenChars}}.
b"([^"]|\\")*" : {token, {byteString, TokenLine, TokenChars}}.
br#"([^"]|\\")*"# : {token, {rawByteString, TokenLine, TokenChars}}.

% Identifiers
{L}+[A-Za-z0-9_]*|{L}+[A-Za-z0-9\.]*|[_][A-Za-z0-9_]+ : {token, {identifier, TokenLine, TokenChars}}.
#\[.*\] : {token, {test, TokenLine, TokenChars}}.

% Comments
//(.)*\n?|///(.|\s)*\n?|//!(.|\s)*\n? : {token, {lineComment, TokenLine, TokenChars}}.
/\*([^*]|\*+[^/]|(/\*([^*]|\*+[^/])*\*/))*\*/ : {token, {blockComment, TokenLine, TokenChars}}.

% Operators
[+/*=~><#':$&!%@^?\|.\-_\\]+ : {token, {operator, TokenLine, TokenChars}}.
[\(\)\{\}\[\];,] : {token, {delimiter, TokenLine, TokenChars}}.

% Macros
{L}+[A-Za-z0-9_]*! : {token, {macro, TokenLine, TokenChars}}.

% Functions
{L}+[A-Za-z0-9_]*\( : {token, {function, TokenLine, TokenChars}}.


Erlang code.